package de.uulm.in.pvs.gui;

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.event.MouseInputAdapter;
import javax.swing.filechooser.FileFilter;
import javax.swing.JViewport; //test

public class ImageViewer implements ActionListener {

	private JFrame frmImageviewer;

	private Icon picture;
	private JLabel imageLabel;
	private DrawingRuler ruler1;
	private DrawingRuler ruler2;
	private JLabel lblFileName;
	private JScrollPane imageScrollPane;
	
	private Point kickposition;
	
	
	
	
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ImageViewer window = new ImageViewer();
					window.frmImageviewer.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ImageViewer() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmImageviewer = new JFrame();
		frmImageviewer.setTitle("ImageViewer");
		frmImageviewer.setBounds(100, 100, 774, 618);
		frmImageviewer.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JMenuBar menuBar = new JMenuBar();
		frmImageviewer.setJMenuBar(menuBar);

		JMenu mnFile = new JMenu("File");
		mnFile.setMnemonic('f');
		menuBar.add(mnFile);

		JMenuItem mntmLoad = new JMenuItem("Load");
		mntmLoad.addActionListener(this);
		mntmLoad.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_L,
				InputEvent.CTRL_MASK));
		mntmLoad.setMnemonic('l');
		mntmLoad.setDisplayedMnemonicIndex(0);
		mnFile.add(mntmLoad);

		JMenuItem mntmOpenUrl = new JMenuItem("Open URL");
		mntmOpenUrl.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String result = (String) JOptionPane.showInputDialog(
						frmImageviewer, "Please enter an URL:",
						"Get Image from URL", JOptionPane.QUESTION_MESSAGE,
						null, null, "http://upload.wikimedia.org/wikipedia/commons/d/d4/Ulmer_muenster_hb.jpg");
				if (result != null) {
					try {
						URL url = new URL(result);
						Object content = url.openConnection().getContent();
						if (content == null) {
							throw new MalformedURLException("No valid URL!");
						}
						picture = new ImageIcon(url);
						lblFileName.setText(url.toExternalForm());
						imageLabel.setIcon(picture);
						ruler1.setPreferredSize(picture);
						ruler2.setPreferredSize(picture);
					} catch (MalformedURLException e1) {
						JOptionPane.showMessageDialog(frmImageviewer,
								"Bad URL format!", "Error",
								JOptionPane.ERROR_MESSAGE);
					} catch (IOException e1) {
						JOptionPane
								.showMessageDialog(
										frmImageviewer,
										"URL does not exist or does not contain a picture!",
										"Error", JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		});
		mntmOpenUrl.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_U,
				InputEvent.CTRL_MASK));
		mnFile.add(mntmOpenUrl);

		JMenuItem mntmSave = new JMenuItem("Save");
		mntmSave.setMnemonic('s');
		mntmSave.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S,
				InputEvent.CTRL_MASK));
		mnFile.add(mntmSave);

		JSeparator separator = new JSeparator();
		mnFile.add(separator);

		JMenuItem mntmExit = new JMenuItem("Exit");
		mntmExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		mntmExit.setMnemonic('e');
		mntmExit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q,
				InputEvent.CTRL_MASK));
		mnFile.add(mntmExit);

		JMenu mnView = new JMenu("View");
		menuBar.add(mnView);

		JMenuItem mntmNextInPath = new JMenuItem("Next in Path");
		mntmNextInPath.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N,
				InputEvent.CTRL_MASK));
		mnView.add(mntmNextInPath);

		JMenuItem mntmNewMenuItem = new JMenuItem("Previous in Path");
		mntmNewMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P,
				InputEvent.CTRL_MASK));
		mnView.add(mntmNewMenuItem);
		
		JMenu mnHelp = new JMenu("Help");
		mnHelp.setMnemonic('h');
		menuBar.add(mnHelp);
		
		JMenuItem mntmHilfe = new JMenuItem("Help");
		mntmHilfe.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_H, InputEvent.CTRL_MASK));
		mnHelp.add(mntmHilfe);
		frmImageviewer.getContentPane().setLayout(new BorderLayout(0, 0));

		JToolBar toolBar = new JToolBar();
		frmImageviewer.getContentPane().add(toolBar, BorderLayout.NORTH);

		JButton btnLoad = new JButton("");
		btnLoad.addActionListener(this);
		btnLoad.setIcon(new ImageIcon(ImageViewer.class
				.getResource("/resources/open.png")));
		toolBar.add(btnLoad);
		
		JButton button = new JButton("");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					ImageIcon image = (ImageIcon)imageLabel.getIcon();
					Rectangle labelSize = imageLabel.getVisibleRect();
					labelSize.width += imageScrollPane.getVerticalScrollBar().getWidth();
					labelSize.height += imageScrollPane.getHorizontalScrollBar().getHeight();
					imageLabel.setIcon(new ImageIcon(this.getScaledImage(image.getImage(),labelSize.width,labelSize.height)));					
				} catch (Exception exc) {}
			}
			
			private Image getScaledImage(Image srcImg, int w, int h){
				return srcImg.getScaledInstance(w, h, Image.SCALE_SMOOTH);
			}
		});
		button.setIcon(new ImageIcon(ImageViewer.class.getResource("/resources/zoom.png")));
		toolBar.add(button);

		JSplitPane splitPane = new JSplitPane();
		splitPane.setResizeWeight(0.8);
		splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
		frmImageviewer.getContentPane().add(splitPane, BorderLayout.CENTER);

		JPanel description = new JPanel();
		splitPane.setRightComponent(description);
		description.setLayout(new BorderLayout(0, 0));

		JLabel lblDescription = new JLabel("description");
		description.add(lblDescription, BorderLayout.NORTH);

		JScrollPane scrollPane_1 = new JScrollPane();
		description.add(scrollPane_1, BorderLayout.CENTER);

		JTextArea txtDescription = new JTextArea();
		txtDescription.setColumns(80);
		txtDescription.setLineWrap(true);
		txtDescription.setFont(new Font("Monospaced", Font.PLAIN, 15));
		scrollPane_1.setViewportView(txtDescription);

		JPanel image = new JPanel();
		splitPane.setLeftComponent(image);
		image.setLayout(new BorderLayout(0, 0));

		lblFileName = new JLabel("no file loaded...");
		image.add(lblFileName, BorderLayout.NORTH);

		imageScrollPane = new JScrollPane();
		imageScrollPane.setViewportBorder(new LineBorder(new Color(0, 0, 0), 5, true));
		ruler1 = new DrawingRuler(DrawingRuler.HORIZONTAL);
		ruler1.setPreferredSize(new Dimension(0, 0));
		ruler2 = new DrawingRuler(DrawingRuler.VERTICAL);
		ruler2.setPreferredSize(new Dimension(0, 0));
		imageScrollPane.setColumnHeaderView(ruler1);
		imageScrollPane.setRowHeaderView(ruler2);
		image.add(imageScrollPane, BorderLayout.CENTER);

		imageLabel = new JLabel();
		imageScrollPane.setViewportView(imageLabel);	
		
		//Per Hand scrollen
		
		/*
		 * Erstellen einen JViewer, der das Bild anzeigt
		 */
		
		final JViewport view = imageScrollPane.getViewport();
		
		/*
		 * Einen MousListener erstellen, der �berpr�ft, ob 
		 * gedr�ckt wurde. Wenn gedr�ckt wird, dann wird der gedr�ckte x und y wert 
		 * der Maus beim dr�cken abgespeichert.
		 */
		
		view.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				Point akv = view.getViewPosition();
				final int akvX = akv.x;
				final int akvY = akv.y;
				
				final Point xy = e.getPoint();
				
				/*
				 * hier wird ein Motionlistener hinzugef�gt,
				 * damit wird den Punkt xy bekommen, der am anfang ged�rckt wurde,
				 * setzen wir den final.
				 */
				
				view.addMouseMotionListener(new MouseMotionListener() {
					
					@Override
					public void mouseMoved(MouseEvent e) {
						// TODO Auto-generated method stub
						 
					}
					
					@Override
					public void mouseDragged(MouseEvent e) {
						
						/*
						 * Rechnung: Bildkoordinate - AnfangsPunktkoordinate - MomentanePunktkoordinate
						 */
						
						int x = e.getX();
						int y = e.getY();
						int x1 = xy.x;
						int y1 = xy.y;
						view.setViewPosition(new Point((akvX - (x-x1)),(akvY - (y-y1))));
					}
				});
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
		
		
		/**view.addMouseMotionListener(new MouseMotionListener() {
			
			@Override
			public void mouseMoved(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseDragged(MouseEvent e) {								
				//view.setViewPosition(e.getPoint());
				Point akp = view.getViewPosition();
				akp.
				Point test = new Point(e.getX(), e.getY());
				view.setViewPosition(test);
			}
		});**/
		
	}
	
	public void actionPerformed(ActionEvent e) {
		JFileChooser filedialog = new JFileChooser();
		filedialog.addChoosableFileFilter(new FileFilter() {
			public boolean accept(File f) {
				if (f.isDirectory()) {
					return true;
				}

				String extension = null;
				String s = f.getName();
				int i = s.lastIndexOf('.');

				if (i > 0 && i < s.length() - 1) {
					extension = s.substring(i + 1).toLowerCase();
				}

				if (extension != null) {
					if (extension.equals("jpg")
							|| extension.equals("jpeg")
							|| extension.equals("gif")
							|| extension.equals("png")) {
						return true;
					}
					else {
						return false;
					}
				}
				return false;
			}

			@Override
			public String getDescription() {
				return "*.gif, *.jpg, *.png";
			}
		});
		filedialog.setAcceptAllFileFilterUsed(false);
		int result = filedialog.showOpenDialog(frmImageviewer);
		if (result == JFileChooser.APPROVE_OPTION) {
			File file = filedialog.getSelectedFile();
			if (file != null) {
				String filename = file.getAbsolutePath();
				picture = new ImageIcon(filename);
				lblFileName.setText(filename);
				imageLabel.setIcon(picture);
				ruler1.setPreferredSize(picture);
				ruler2.setPreferredSize(picture);
			}
		}
	}
}
