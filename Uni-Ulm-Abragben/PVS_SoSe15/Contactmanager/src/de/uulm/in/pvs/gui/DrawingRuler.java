package de.uulm.in.pvs.gui;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Rectangle;

import javax.swing.Icon;
import javax.swing.JComponent;

public class DrawingRuler extends JComponent {

	public static final int HORIZONTAL = 0;
	public static final int VERTICAL = 1;

	public static final int SIZE = 30;
	private static final int UNIT = 50;

	private int direction;

	DrawingRuler(int direction) {
		this.direction = direction;
	}

	protected void setPreferredSize(Icon icon) {
		if (icon != null) {
			if (this.direction == HORIZONTAL) {
				setPreferredSize(new Dimension(icon.getIconWidth(), SIZE));
			} else {
				setPreferredSize(new Dimension(SIZE, icon.getIconHeight()));
			}

		}
	}

	@Override
	protected void paintComponent(Graphics g) {
		Rectangle area = g.getClipBounds();

		int start = (direction == HORIZONTAL) ? area.x : area.y;
		int end = start
				+ ((direction == HORIZONTAL) ? area.width : area.height);

		for (int i = start; i <= end; i++) {
			if (i % UNIT == 0) {
				String text = Integer.toString(i);
				if (direction == HORIZONTAL) {
					g.drawLine(i, 20, i, SIZE);
					g.drawString(text, i, 15);
				} else {
					g.drawLine(20, i, SIZE, i);
					g.drawString(text, 0, i + 15);
				}
			}
		}
	}

	/**
	 * @return the direction
	 */
	public int getDirection() {
		return direction;
	}

	/**
	 * @param direction
	 *            the direction to set. Possible values:
	 *            DrawingRuler.HORZIONTAL, DrawingRuler.VERTICAL
	 */
	public void setDirection(int direction) {
		this.direction = direction;
	}
}
