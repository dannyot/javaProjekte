package observerpattern;
import java.util.*;


public class SchwarzesBrett extends Observable {
	
    
	
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		SchwarzesBrett brett = new SchwarzesBrett();
		System.out.println("Bitte Name eingeben zum anmelden: ");
		String text = scan.nextLine();
		brett.addOb(text);
		
		
		System.out.println("Erzaehle etwas: ");
		text = scan.nextLine();
		
		brett.changeMessage(text);
		
		
	}
	
	public void addOb(String name){
		this.addObserver(new Student(name));
	}
	
	public void changeMessage(String message){
		setChanged();
		notifyObservers(message);
	}
}
