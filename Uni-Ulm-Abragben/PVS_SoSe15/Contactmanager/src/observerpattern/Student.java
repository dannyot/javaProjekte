package observerpattern;
import java.util.*;

import javax.swing.*;

public class Student implements Observer  {
	JFrame frame;
	JTextField nachricht;
	
	Student(String name){
		frame = new JFrame(name);
		nachricht = new JTextField("noch leer");
		frame.add(nachricht);
		
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
        frame.setSize(300, 300); 
        frame.setVisible(true);
	}
	
	
	@Override
	public void update(Observable o, Object arg) {
		nachricht.setText((String) arg);
	}

}
