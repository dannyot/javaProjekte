CREATE TABLE contacts (
	id INT NOT NULL IDENTITY,
	name VARCHAR(200),
	address VARCHAR(200),
	phone VARCHAR(200),
	email VARCHAR(200)
);

CREATE TABLE dates (
	id INT NOT NULL,
	date DATE,
	FOREIGN KEY(id) REFERENCES contacts(id)
);

INSERT INTO Contacts
VALUES(NULL,'name1','address1','phone1','name1@uni-ulm.de');
INSERT INTO Contacts
VALUES(NULL,'name2','address2','phone2','name2@uni-ulm.de');
INSERT INTO Contacts
VALUES(NULL,'name3','address3','phone3','name3@uni-ulm.de');
INSERT INTO Contacts
VALUES(NULL,'name4','address4','phone4','name4@uni-ulm.de');
INSERT INTO Contacts
VALUES(NULL,'name5','address5','phone5','name5@uni-ulm.de');
