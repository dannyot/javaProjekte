package uni.ulm.test;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;

/**
 *
 * @author elsholz
 * @see http://www.exampledepot.com/egs/javax.xml.transform/WriteDom.html
 */
public class DocumentWriter {

	/**
	 * prevent initialization
	 */
	private DocumentWriter() {}

	/**
	 * Writes a {@link Document} to a {@link File}.
	 * @param doc the {@link Document} to be written
	 * @param file the {@link File} to write to
	 * @throws NullPointerException if doc or file is null
	 */
	public static void write(Document doc, File file) throws NullPointerException {
		if (null == doc || null == file) throw new NullPointerException();
		// Prepare the DOM document for writing
        Source source = new DOMSource(doc);
        // Prepare the output file
        Result result = new StreamResult(file);
        Transformer xformer;
		try {
			xformer = TransformerFactory.newInstance().newTransformer();
	        // Write the DOM document to the file
	        xformer.transform(source, result);
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerFactoryConfigurationError e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		}
	}

	/**
	 * debug purpose only
	 * @param args
	 * @throws ParserConfigurationException
	 */
	public static void main(String[] args) throws ParserConfigurationException {
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.newDocument();
		
		File file = new File("contacts.xml");
		write(doc, file);
	}
}
