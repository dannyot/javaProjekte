package layout.manager;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

/*
 * BorderLayout.
 * Als Grundger�st nehmen wir ein BorderLayout, im Kopf das Label stehen hat
 * und im Center ein Gridlayout mit dem Rechner besitzt
 */
public class Border {
	public static void main(String args[]){
		/*
		 * Frame wird erstellt, in dem alles abl�uft.
		 */
		JFrame rechner = new JFrame();
		
		rechner.setLayout(new BorderLayout());
		
		JLabel top = new JLabel("0");
		top.setHorizontalAlignment(SwingConstants.RIGHT);
		
		rechner.add(top, BorderLayout.NORTH);
		
		JPanel tastenFeld = new JPanel();
		tastenFeld.setLayout(new GridLayout(4, 4,2,2));
		
		/*
		 * Array mit Werten der Buttons werden erstellt.
		 */
		String[] zuweisung = {"+","1","2","3","-","4","5","6","x","7","8","9",":","0","=","C"};
		/*
		 * ButtonArray wird erstellt.
		 */
		JButton[] buttons = new JButton[16];
		/*
		 * Jedem erstellten Button wird das gleiche element von zuweisung zugewisen.
		 * Dadurch erspart mach sich das erstellen der ganzen Buttons durch die Hand.
		 */
		for(int i = 0; i<buttons.length; i++){
			buttons[i] = new JButton(zuweisung[i]);
			buttons[i].setPreferredSize(new Dimension(50, 30));
			tastenFeld.add(buttons[i]);
		}
		
		
		rechner.add(tastenFeld, BorderLayout.CENTER);
		
		rechner.pack();
		rechner.setVisible(true);
	}
}
