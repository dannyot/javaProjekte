package layout.manager;
import javax.swing.*;

import java.awt.*;
/*
 * Bei dem Flowlayout kommt es auf die Gr�se an. Beim ver�ndern der Gr��e des Fensters, wird auch das 
 * Aussehen drastisch ge�ndert.
 */
public class Flow {
	public static void main(String args[]){
		JFrame frame = new JFrame("Rechner");
		frame.setLayout(new FlowLayout());
		frame.setPreferredSize(new Dimension(250,250));
		
		JLabel nul = new JLabel("0");
		nul.setHorizontalAlignment(SwingConstants.RIGHT);
		nul.setPreferredSize(new Dimension(200, 30));
		frame.add(nul);
		
		String[] zuweisung = {"+","1","2","3","-","4","5","6","x","7","8","9",":","0","=","C"};
		JButton[] buttons = new JButton[16];
		
		for(int i = 0; i<buttons.length; i++){
			buttons[i] = new JButton(zuweisung[i]);
			buttons[i].setPreferredSize(new Dimension(50, 30));
			frame.add(buttons[i]);
		}
		frame.pack();
		frame.setVisible(true);
	}
}
