package layout.manager;
import javax.swing.*;
import java.awt.*;

/*
 * Gridlayout mit 1ner Spalte und 1ner Reihe indem ein Borderlayot ist.
 * Das Borderlayout ist wie in Border.
 */
public class Grid extends JFrame{
	public static void main(String args[]){
		JFrame rechner = new JFrame();
		
		rechner.setLayout(new GridLayout(1, 1));
		
		JPanel richtigerRechner = new JPanel();
		richtigerRechner.setLayout(new BorderLayout());
		
		JLabel top = new JLabel("0");
		top.setHorizontalAlignment(SwingConstants.RIGHT);
		
		richtigerRechner.add(top, BorderLayout.NORTH);
		
		JPanel tastenFeld = new JPanel();
		tastenFeld.setLayout(new GridLayout(4, 4,2,2));
		
		String[] zuweisung = {"+","1","2","3","-","4","5","6","x","7","8","9",":","0","=","C"};
		JButton[] buttons = new JButton[16];
		
		for(int i = 0; i<buttons.length; i++){
			buttons[i] = new JButton(zuweisung[i]);
			buttons[i].setPreferredSize(new Dimension(50, 30));
			tastenFeld.add(buttons[i]);
		}
		
		
		richtigerRechner.add(tastenFeld, BorderLayout.CENTER);
		
		rechner.add(richtigerRechner);
		
		rechner.pack();
		rechner.setVisible(true);
	}
}
