package contactmanager.data;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import org.w3c.dom.Attr;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.Text;

import contactmanager.exeption.EmailException;
import contactmanager.util.*;

public class Contact implements Comparable<Contact>, Serializable {
	public String name;
	public String address;
	public String phone;
	private String email;
	private int id;

	/*
	 * Konstruktor wurde geschrieben, damit die Parameter (name, address, phone,
	 * email) schnell gesetzt werden koennen.
	 */

	public Contact(int id, String name, String address, String phone, String email)
			throws EmailException {
		this.id = id;
		this.name = name;
		this.address = address;
		this.phone = phone;
		setEmail(email);
	}
	public Contact(String name, String address, String phone, String email)
			throws EmailException {
		this.id = 0;
		this.name = name;
		this.address = address;
		this.phone = phone;
		setEmail(email);
	}

	public Contact() {
		try {
			this.name = "test";
			this.address = "test!";
			this.phone = "test";
			setEmail("tes@test.de");
		} catch (EmailException m) {
		}
	}

	/*
	 * setEmail prueft vor dem setzen dem Emailadressen, ob diese Emailadresse
	 * ueberhaupt existieren kann
	 */
	public void setEmail(String mail) throws EmailException {
		if (InputChecker.checkEMailAddress(mail)) {
			this.email = mail;
		} else {
			throw new EmailException();
		}
	}

	/*
	 * Getter
	 */
	public String getEmail() {
		return this.email;
	}

	/*
	 * Es werden die Variablen verglichen. Ein Kontakt ist gleich, wenn alle
	 * Daten gleich sin.
	 */
	public boolean equals(Contact c) {
		return (c.name == this.name && c.address == this.address
				&& c.phone == this.phone && c.getEmail() == this.email);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return this.name;
	}
	
	/**
	 * Aufgabe 11 Neu eingfügt Anfang:
	 */
	public void setId(int i){
		this.id = i;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public int getID(){
		return this.id;
	}
	
	
	/**
	 * Neu eingefügt ENDE
	 */

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((phone == null) ? 0 : phone.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see contactmanager.data.Comparable#compareTo(java.lang.Object)
	 * CompareTo, die im Interface schon deklariert wurde, wird hier
	 * �berschrieben.
	 */
	@Override
	public int compareTo(Contact c) {
		return this.name.compareTo(c.name);
	}
	/*
	 * Frage f�r Tut, warum funktioniert es mit der Standart Funktion, aber nicht mit der selbst geschriebenen?
	 */
	/**public void writeObject(ObjectOutputStream oOutput) throws IOException {
		oOutput.writeObject(name);
		oOutput.writeObject(address);
		oOutput.writeObject(phone);
		oOutput.writeObject(email);
	}

	private void readObject(ObjectInputStream stream) throws IOException,
			ClassNotFoundException {
		name = (String) stream.readObject();
		address = (String) stream.readObject();
		phone = (String) stream.readObject();
		email = (String) stream.readObject();
	}**/
	public Node toXML(Document doc){
		Element root = doc.createElement("Contact");
		doc.getDocumentElement().appendChild(root);
		
		Element name = doc.createElement("Name");
		Text n = doc.createTextNode(this.name);
		name.appendChild(n);
		Element addr = doc.createElement("Address");
		Text a = doc.createTextNode(this.address);
		addr.appendChild(a);
		Element phone = doc.createElement("Phone");
		Text p = doc.createTextNode(this.phone);
		phone.appendChild(p);
		Element email = doc.createElement("Email");
		Text e = doc.createTextNode(this.email);
		email.appendChild(e);
		
		root.appendChild(name);
		root.appendChild(addr);
		root.appendChild(phone);
		root.appendChild(email);
		
		return doc.getDocumentElement();
		
		
	}
	
	public static Contact create(Node n){
		if(n.getChildNodes().item(0)!=null && n.getChildNodes().item(3)!=null){
			Contact c = new Contact();
			c.name = n.getChildNodes().item(0).getTextContent();
			c.address = n.getChildNodes().item(1).getTextContent();
			c.phone = n.getChildNodes().item(2).getTextContent();
			c.email = n.getChildNodes().item(3).getTextContent();
			return c;
		}
		return null;
	}
	

/*
 * DTD unserer XML-Datei
 */
/**
<!ELEMENT directory (Contact)*>
<!ELEMENT Contact (Name,Address,Phone,Email)>
<!ELEMENT Name (#PCDATA)>
<!ELEMENT Address (#PCDATA)>
<!ELEMENT Phone (#PCDATA)>
<!ELEMENT Phone (#PCDATA)>
**/

}

/*
 * Interface, dass von Compareable erbt. Hier wird die Funktion compareTo in
 * Compareable �berschrieben.
 */
interface Comparable<Contact> extends java.lang.Comparable<Contact> {
	@Override
	public int compareTo(Contact c);
}
