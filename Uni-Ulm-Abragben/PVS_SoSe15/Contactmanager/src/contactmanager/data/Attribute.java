package contactmanager.data;

// enum to differentiate the different Contact attributes
public enum Attribute{NAME, ADDR, PHONE, EMAIL}
