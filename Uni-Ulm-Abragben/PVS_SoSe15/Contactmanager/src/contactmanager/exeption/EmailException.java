package contactmanager.exeption;

public class EmailException extends Exception
{
    // Konstruktor unserer eigenen Exception
    public EmailException()
    {
        // Aufruf des übergeordneten Konstruktors mit dem zu
        // erscheinenden Fehlertext
        super("dies ist keine gültige Emailadresse");
    }
}