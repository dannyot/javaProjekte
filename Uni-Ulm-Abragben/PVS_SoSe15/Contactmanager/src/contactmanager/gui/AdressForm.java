package contactmanager.gui;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.security.AllPermission;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;

import contactmanager.ctrl.ContactManagerMain;
import contactmanager.exeption.EmailException;
import contactmanager.data.Contact;

/*
 * Borderlayout, mit einem Grid im Center und im South haben wir dann die icons. Das Grid
 * im Center hat wiederum ein Borderlayout, indem nur west und center beutzt werden (west ist
 * kleiner als center).
 */
public class AdressForm {
	/**
	 * @wbp.parser.entryPoint
	 */
	private int aktuellenummer = 0;
	private Contact aktuellerContact;

	public void start() {
		final FileFilter csvFilter = new CsvFilter();
		final FileFilter datFilter = new DatFilter();
		final FileFilter xmlFilter = new XmlFilter();
		JFrame frame = new JFrame("Contacmanager");
		final JFrame saveAsDialog = new JFrame("Save as");
		frame.setLayout(new BorderLayout());

		JPanel mitte = new JPanel();
		mitte.setLayout(new GridLayout(4, 1, 20, 20));

		// frame.setLayout(new GridLayout(4, 1, 20 ,20));
		String[] zuweisung = { "Name:", "Addresse:", "Phone:", "Email:" };
		JLabel[] test = new JLabel[4];
		final JTextArea[] text = new JTextArea[4];
		JPanel[] panel = new JPanel[4];
		JScrollPane[] scroll = new JScrollPane[4];

		/*
		 * Textfelder werden gesetzt.
		 */

		for (int i = 0; i < text.length; i++) {
			panel[i] = new JPanel();
			panel[i].setLayout(new BorderLayout());
			test[i] = new JLabel(zuweisung[i]);
			test[i].setFont(new Font("Arial", Font.BOLD, 15));
			test[i].setHorizontalAlignment(SwingConstants.CENTER);
			test[i].setPreferredSize(new Dimension(150, 100));
			panel[i].add(test[i], BorderLayout.WEST);
			text[i] = new JTextArea();
			text[i].setLineWrap(true);
			text[i].setWrapStyleWord(true);
			// text[i].setPreferredSize(new Dimension(350,100));
			scroll[i] = new JScrollPane(text[i]);
			scroll[i].setPreferredSize(new Dimension(350, 100));
			panel[i].add(scroll[i], BorderLayout.CENTER);
			mitte.add(panel[i]);
		}
		JPanel unten = new JPanel();
		unten.setLayout(new GridLayout(1, 5, 70, 70));
		String[] source = { "arrow_full_left_32.png", "cancel_32.png",
				"plus_32.png", "floppy_disk_32.png", "arrow_full_right_32.png" };
		final JButton[] bilder = new JButton[source.length];

		/*
		 * Buttons werden gesetzt
		 */

		for (int i = 0; i < source.length; i++) {
			bilder[i] = new JButton();
			bilder[i].setIcon(new ImageIcon(getClass().getResource(
					"./res/" + source[i])));
			bilder[i].setPreferredSize(new Dimension(40, 40));
			unten.add(bilder[i]);
		}
		
		
		

		bilder[0].setEnabled(false);
		bilder[source.length - 1].setEnabled(false);

		bilder[0].addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Contact c = ContactManagerMain.before();
				bilder[4].setEnabled(true);
				if (c == null) {
					JOptionPane
							.showMessageDialog(null,
									"Sie befinden sich schon am Anfang der Kontakt-Liste");
				} else {
					String[] ole = { c.name, c.address, c.phone, c.getEmail() };
					for (int i = 0; i < ole.length; i++) {
						text[i].setText(ole[i]);
					}
					bilder[2].setEnabled(false);

				}
			}
		});
		bilder[4].addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Contact c = ContactManagerMain.behind();
				if (c == null) {
					for (int i = 0; i < 4; i++) {
						text[i].setText("");
					}
					bilder[2].setEnabled(true);
					bilder[4].setEnabled(false);
				} else {
					String[] ole = { c.name, c.address, c.phone, c.getEmail() };
					for (int i = 0; i < ole.length; i++) {
						text[i].setText(ole[i]);
					}
					bilder[2].setEnabled(false);
				}
			}
		});

		bilder[2].addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				ContactManagerMain.addDB(text[0].getText(), text[1].getText(),
						text[2].getText(), text[3].getText());
				bilder[0].setEnabled(true);
				bilder[4].setEnabled(true);
				for (int i = 0; i < 4; i++) {
					text[i].setText("");
				}
			}
		});

		bilder[1].addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Contact c = ContactManagerMain.delete();
				if (c == null) {
					for (int i = 0; i < 4; i++) {
						text[i].setText("");
					}
					bilder[2].setEnabled(true);
					bilder[4].setEnabled(false);

				} else {
					String[] ole = { c.name, c.address, c.phone, c.getEmail() };
					for (int i = 0; i < ole.length; i++) {
						text[i].setText(ole[i]);
					}
					bilder[2].setEnabled(false);
					bilder[4].setEnabled(true);
					
				}

			}
		});

		bilder[3].addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				String name = text[0].getText();
				String address = text[1].getText();
				String phone = text[2].getText();
				String email = text[3].getText();
				try{
					ContactManagerMain.saveDB(new Contact(name,address,phone,email));
				} catch(Exception e){
					JOptionPane.showMessageDialog(null, "Die Aktion konnte nicht ausgeführt werden, da irgendwas mit der Mail nicht stimmt!");
				}
			}
		});

		JMenuBar menue = new JMenuBar();
		JMenu file = new JMenu("File");
		JMenuItem open = new JMenuItem("Open Address Book");
		open.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				/*JFileChooser choosFile = new JFileChooser();

				choosFile.setFileFilter(csvFilter);
				choosFile.setFileFilter(datFilter);
				choosFile.setFileFilter(xmlFilter);
				choosFile.addChoosableFileFilter(csvFilter);
				choosFile.addChoosableFileFilter(datFilter);
				choosFile.addChoosableFileFilter(datFilter);

				int temp = choosFile.showOpenDialog(saveAsDialog);

				if (temp == JFileChooser.APPROVE_OPTION) {
					File maehhh = choosFile.getSelectedFile();
					Contact c = null;
					if (csvFilter.accept(maehhh)) {
						c = ContactManagerMain.loadContacts(maehhh);
					} else if (datFilter.accept(maehhh)) {
						c = ContactManagerMain.loadBinaryStream(maehhh);
					} else if (xmlFilter.accept(maehhh)){
						c = ContactManagerMain.loadXML(maehhh);
					}
					
					if (c == null) {
						JOptionPane
								.showMessageDialog(null,
										"Sie befinden sich schon am Anfang der Kontakt-Liste");
					} else {
						String[] ole = { c.name, c.address, c.phone,
								c.getEmail() };
						for (int i = 0; i < ole.length; i++) {
							text[i].setText(ole[i]);
						}

					}*/
				Contact c = ContactManagerMain.loadDB();
				String[] ole = { c.name, c.address, c.phone,
						c.getEmail() };
				for (int i = 0; i < ole.length; i++) {
					text[i].setText(ole[i]);
				}
				bilder[0].setEnabled(true);
				bilder[4].setEnabled(true);
				

			}
		});

		JMenuItem create = new JMenuItem("Create New Address Book");

		create.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				save();
				//ContactManagerMain.loadContacts(null); <-ALT
				ContactManagerMain.deleteALL();
				for (JTextArea e : text) {
					e.setText("");
				}
			}
		});

		JMenuItem save = new JMenuItem("Save as...");
		save.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				JOptionPane.showMessageDialog(null, "Diese Funktion ist auser Betrieb");
				/*JFileChooser saveAs = new JFileChooser("Speichern als");

				saveAs.setFileFilter(csvFilter);
				saveAs.setFileFilter(datFilter);
				saveAs.setFileFilter(xmlFilter);
				saveAs.addChoosableFileFilter(csvFilter);
				saveAs.addChoosableFileFilter(datFilter);
				saveAs.addChoosableFileFilter(xmlFilter);

				int t = saveAs.showSaveDialog(saveAsDialog);

				if (t == JFileChooser.APPROVE_OPTION) {
					File fileToSave = saveAs.getSelectedFile();
					if (csvFilter.accept(fileToSave)) {

						ContactManagerMain.saveContacts(fileToSave);
					} else if (datFilter.accept(fileToSave)) {
						ContactManagerMain.saveBinaryStream(fileToSave);
					} else if(xmlFilter.accept(fileToSave)){
						ContactManagerMain.saveXML(fileToSave);
					}
					else {
						JOptionPane
								.showMessageDialog(null,
										"Sie haben kein vorgesehenes Dateiformat ausgew�hlt!");
					}
				}*/
			}
		});
		JMenuItem exit = new JMenuItem("Exit");
		file.add(open);
		file.add(create);
		file.add(save);
		file.add(exit);
		menue.add(file);

		// menuelistener

		exit.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);

			}
		});

		frame.add(mitte, BorderLayout.CENTER);
		frame.add(unten, BorderLayout.SOUTH);
		frame.add(menue, BorderLayout.NORTH);
		frame.pack();
		
		frame.addWindowListener(new WindowAdapter() {
			@Override
	        public void windowClosing(WindowEvent e){
	            System.out.println("Closed");
	            System.exit(1);
	            e.getWindow().dispose();
	        }
	        
		});
		
		frame.setVisible(true);
	}

	public void save() {
		if (ContactManagerMain.saveFolder == null) {
			JOptionPane
					.showMessageDialog(null,
							"Sie muessen erst einen Speicherort bestimmen, oder aus einer Datei laden.");
		} else {
			if(new CsvFilter().accept(ContactManagerMain.saveFolder)){
			ContactManagerMain.saveContacts(ContactManagerMain.saveFolder);
			}
			else if(new DatFilter().accept(ContactManagerMain.saveFolder)){
				ContactManagerMain.saveBinaryStream(ContactManagerMain.saveFolder);
			}
			else if(new XmlFilter().accept(ContactManagerMain.saveFolder)){
				ContactManagerMain.saveXML(ContactManagerMain.saveFolder);
			}
			else{
				JOptionPane
				.showMessageDialog(null,
						"Irgendwas lief schief bitte schreiben sie keinen Admin an");
			}
		}
	}
}
