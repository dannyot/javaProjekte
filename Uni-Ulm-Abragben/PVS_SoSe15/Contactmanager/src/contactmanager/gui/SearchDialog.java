package contactmanager.gui;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridLayout;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;

import contactmanager.data.Attribute;
import contactmanager.data.Contact;

public class SearchDialog extends JDialog {

	private static final long serialVersionUID = -5825289821358720426L;
	private static final int WIDTH = 700;
	private static final int HEIGHT = 500;
	
	private JTextField searchField;
	private JList<Contact> contactList;
	private JButton selectContactButton;
	private JButton cancelButton;
	private JRadioButton nameButton;
	private JRadioButton addressButton;
	private JRadioButton phoneButton;
	private JRadioButton emailButton;
	
	public SearchDialog(Frame owner) {
		super(owner, true);
		setSize(WIDTH, HEIGHT);
		setTitle("Search Contacts");
		setResizable(false);
		setLocationRelativeTo(getRootPane()); 
		Container content = getContentPane();	
		content.setLayout(new BorderLayout());
		
		// search panel containing radio buttons and a text field 
		JPanel searchPanel = new JPanel();
		searchPanel.setLayout(new GridLayout(2,1));
		
		// panel with radio buttons that are grouped together in a ButtonGroup
		JPanel radioButtonPanel = new JPanel();
		radioButtonPanel.setLayout(new GridLayout(1,1));
		ButtonGroup radioButtons = new ButtonGroup();
		nameButton = new JRadioButton("Name");
		addressButton = new JRadioButton("Address");
		phoneButton = new JRadioButton("Phone");
		emailButton = new JRadioButton("E-Mail");
		
		radioButtons.add(nameButton);
		radioButtons.add(addressButton);
		radioButtons.add(phoneButton);
		radioButtons.add(emailButton);
		
		radioButtonPanel.add(nameButton);
		radioButtonPanel.add(addressButton);
		radioButtonPanel.add(phoneButton);
		radioButtonPanel.add(emailButton);
		
		nameButton.setSelected(true);
		
		// search text field
		searchPanel.add(radioButtonPanel);
		searchField = new JTextField();
		searchPanel.add(searchField);
		content.add(searchPanel, BorderLayout.NORTH);
		
		// scrollpane with a list containing the search hits 
		contactList = new JList<Contact>();
		contactList.setCellRenderer(new ContactCellRenderer());
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.getViewport().setView(contactList);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		contactList.setPreferredSize(new Dimension(WIDTH -20, HEIGHT));
		
		content.add(scrollPane, BorderLayout.CENTER);
		
		// button panel with Select and Cancel Button
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new GridLayout(1,2));
		selectContactButton = new JButton("Select Contact");
		cancelButton = new JButton("Cancel");

		buttonPanel.add(selectContactButton);
		buttonPanel.add(cancelButton);
		content.add(buttonPanel, BorderLayout.SOUTH);
	}
	
	public Attribute getSelectedAttribute() {
		if(nameButton.isSelected())
			return Attribute.NAME;
		else if(addressButton.isSelected())
			return Attribute.ADDR;
		else if(phoneButton.isSelected())
			return Attribute.PHONE;
		else if(emailButton.isSelected())
			return Attribute.EMAIL;
		else {
			nameButton.setSelected(true);
			return Attribute.NAME;
		}
	}
	
	public void clearDialog() {
		contactList.clearSelection();
		Contact[] empty = {};
		contactList.setListData(empty);
		searchField.setText("");
	}
}
