package contactmanager.gui;

import java.awt.Component;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;

import contactmanager.data.Contact;

public class ContactCellRenderer extends JPanel implements ListCellRenderer<Contact>  {

	private static final long serialVersionUID = -7655159316842809728L;

	private JLabel name;
	private JLabel address;
	private JLabel phone;
	private JLabel email;
	
	public ContactCellRenderer() {
		name = new JLabel();
		address = new JLabel();
		phone = new JLabel();
		email = new JLabel();
		setLayout(new GridLayout(1,4));
		add(name);
		add(address);
		add(phone);
		add(email);
	}
	
	@Override
	public Component getListCellRendererComponent(
			JList<? extends Contact> list, Contact contact, int index,
			boolean isSelected, boolean cellHasFocus) {
		
        name.setText(contact.name);
        address.setText(contact.address);
        phone.setText(contact.phone);
        email.setText(contact.getEmail());
        
        if (isSelected) {
        	setBackground(list.getSelectionBackground());
        	setForeground(list.getSelectionForeground());
        } else {
        	setBackground(list.getBackground());
        	setForeground(list.getForeground());
        }
        setEnabled(list.isEnabled());
        setFont(list.getFont());
        setOpaque(true);
		return this;
	}

}
