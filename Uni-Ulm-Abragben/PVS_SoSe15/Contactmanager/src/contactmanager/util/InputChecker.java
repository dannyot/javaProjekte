/**
 * 
 */
package contactmanager.util;

import java.util.regex.Pattern;

/**
 * @author Christian Spann
 * 
 */
public class InputChecker {

	/**
	 * Angepasstes regex pattern von der Seite www.regular-expressions.info zum
	 * validieren von E-Mail Adressen.
	 */
	private static final Pattern pattern = Pattern
			.compile("[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}");

	/**
	 * �berpr�ft ob eine E-Mail Adresse valide ist.
	 * 
	 * @param address
	 *            Die Adresse als String
	 * @return true wenn valid, sonst false
	 */
	public static boolean checkEMailAddress(String address) {
		return pattern.matcher(address).matches();
	}

	/**
	 * Simpler test in der main Methode.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		String address = "christian.spann@uni-ulm.de";
		System.out.println("E-Mail Adresse " + address + " ist "
				+ (checkEMailAddress(address) ? "valide" : "nicht valide"));
	}

}
