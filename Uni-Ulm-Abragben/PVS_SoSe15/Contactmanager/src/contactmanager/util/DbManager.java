package contactmanager.util;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import contactmanager.data.*;

public final class DbManager {
	public static final String URL = "jdbc:hsqldb:file:src/cmdb/contacts.db";
	public static final String DRIVER = "org.hsqldb.jdbcDriver";
	
	static {
		try {
			// load driver
			Class.forName(DRIVER);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	private DbManager() {
		// prevent initialization
	}

	// open and return connnection to DB
	public static Connection openConnection() throws SQLException {
		Connection con=null;
		try {
			con = DriverManager.getConnection(URL,"SA","");
		} catch (SQLException e) {
			e.printStackTrace();
		} 
		return con;
	}
	
	// load contacts from DB and return list
	public static List<Contact> loadContacts() {
		PreparedStatement pstmtLoad;
		try{
			System.out.println("startLoad");
			Connection con = openConnection();
			
			//Statement stmtSelect = con.createStatement();
			
			String statement = "SELECT * FROM contacts";
			
			pstmtLoad = con.prepareStatement(statement);
			
			ResultSet rs = pstmtLoad.executeQuery();
			
			List<Contact> contacts = new ArrayList<Contact>();
			
			contacts.add(null);
			while (rs.next()) {
				int id = rs.getInt(1);
				String name = rs.getString(2);
				String adrress = rs.getString(3);
				String phone = rs.getString(4);
				String email = rs.getString(5);
				
				
				Contact contact = new Contact(id, name, adrress, phone, email);
				contacts.add(contact);
			}
			con.close();
			pstmtLoad.close();
			System.out.println("ENDLOAD");
			return contacts;
		} catch(Exception e){
			System.out.println(e.getMessage());
			return null;
		}
	}
	
	// printing contacts on console (no DB modifications)
	private static void printContacts() throws SQLException{
		Connection con= openConnection();
		Statement stmtSelect = con.createStatement();
		String sql = "SELECT * FROM contacts";
		ResultSet rs = stmtSelect.executeQuery(sql);
		System.out.println(String.format("%6s | %-20s | %-20s | %-20s | %-20s",
				"ID", "Name", "Adresse", "Phone", "Email"));
		
		while (rs.next()) {
			int id = rs.getInt(1);
			String name = rs.getString(2);
			String adrress = rs.getString(3);
			String phone = rs.getString(4);
			String email = rs.getString(5);

			System.out.println(String.format(
					"%6s | %-20s | %-20s | %-20s | %-20s",
					id, name, adrress, phone, email));
		}
		con.close();
		stmtSelect.close();
	}

	
	// printing dates on console (no DB modifications)
	private static void printDates() throws SQLException{
		Connection con= openConnection();
		Statement stmtSelect = con.createStatement();
        String sql = "SELECT * FROM dates";
        ResultSet rs = stmtSelect.executeQuery(sql);
        System.out.println(String.format("%6s | %-20s",
                "ID", "Dates"));
        
        while (rs.next()) {
            int id = rs.getInt(1);
            String name = rs.getString(2);
            String dates = rs.getString(2);

            System.out.println(String.format(
                    "%6s | %-20s",
                    id, dates));
        }
        con.close();
        stmtSelect.close();
	}
	
	// insert contact in db, generate new id
	public static int insertContact(Contact contact) throws SQLException{
		Statement stmtInsert;
		PreparedStatement pstmtInsert;
		
		//System.out.print(contact);


		// Insert an entry
		try {
			System.out.println("StatInsert");
			Connection con= openConnection();
			
			String statement = "Insert INTO contacts (name, address, phone, email) VALUES"
					+ "( ? , ? , ? , ? )";
			
			pstmtInsert = con.prepareStatement(statement);
			
			pstmtInsert.setString(1, contact.name);
			pstmtInsert.setString(2, contact.address);
			pstmtInsert.setString(3, contact.phone);
			pstmtInsert.setString(4, contact.getEmail());
			
			pstmtInsert.execute();
			
			stmtInsert = con.createStatement();
			
			//String werte="'"+contact.name+"','"+contact.address+"','"+contact.phone+"','"+contact.getEmail()+"'";
			//int rowsAffected = stmtInsert
			//		.executeUpdate("INSERT INTO contacts (name, address, phone, email) VALUES("+werte+")");
			
			//Statement stmtSelect = con.createStatement();
	        
			
			statement = "SELECT id FROM contacts ORDER BY id DESC LIMIT 1";
	        
	        pstmtInsert = con.prepareStatement(statement);
	        
	        ResultSet rs = pstmtInsert.executeQuery();
	        
	        rs.next();
	        int id = rs.getInt(1);
			
	        /**
	         * Ohne Sicherheit
	         */
	        //String werteDates = "'"+id+"',CURRENT_DATE";
			//int rowsAffected2 = stmtInsert
			//		.executeUpdate("INSERT INTO dates VALUES ("+werteDates+")");
			//
			
	        statement = "INSERT INTO dates VALUES( ? ,CURRENT_DATE)";
			
			pstmtInsert = con.prepareStatement(statement);
			
			pstmtInsert.setInt(1, id);
			
			pstmtInsert.execute();
			
			con.close();
			
			pstmtInsert.close();
			
			System.out.println("ENDINSERT");
			return id;
		
		} catch (SQLException e) {
			System.err.println("Exception on Insert: " + e.toString());
			return 0;
		}
		
	}
	
	
	// delete contact with given id
       public static void deleteContact(int id){
        Statement stmtDelete;
    
        try {
        	System.out.println("startDELETe");
        	Connection con= openConnection();
        	stmtDelete = con.createStatement();
        	
            int rowsAffected2 = stmtDelete
                    .executeUpdate("DELETE FROM dates WHERE ID="+id);
            int rowsAffected = stmtDelete
                    .executeUpdate("DELETE FROM contacts WHERE ID="+id);

            System.out.println("Rows affected: " + rowsAffected);
            System.out.println("Rows affected: " + rowsAffected2);
            con.close();
            stmtDelete.close();
            System.out.println("ENDDELETE");
        } catch (SQLException e) {
            System.err.println("Exception on Insert: " + e.toString());
        }
    }
	
	// delete all dates and contacts
	public static void deleteAllContacts(){
		Statement stmtInsert;		
		
		try{
			System.out.println("startDELETEALL");
			Connection con= openConnection();
			stmtInsert = con.createStatement();
			
			int rowsAffected = stmtInsert
					.executeUpdate("DELETE FROM dates");
			int rowsAffected2 = stmtInsert
					.executeUpdate("DELETE FROM contacts");
			con.close();
			stmtInsert.close();
			System.out.println("ENDDELETEALL");
		} catch(Exception e){
			System.out.print(e.getMessage());
		}
		
	}
	
	// update the given contact in the DB
	public static void updateContact(Contact contact){
		PreparedStatement stmtUpdate;
		try{
			System.out.println("startUPDATE");
			Connection con= openConnection();
			System.out.println(contact.getID());
			
			
			int id = contact.getID();
		
			/**
			 * ALT Ohne schutz!
			 */
			//String werte = "name='"+contact.name+"',address='"+contact.address+"',phone='"+contact.phone+"',email='"+contact.getEmail()+"'";
			//System.out.println("UPDATE contacts(name,address,phone,email) VALUES("+") WHERE id=");
			//stmtInsert = con.prepareStatment("UPDATE contacts set "+werte+" WHERE id="+id);
			
			String statement = "UPDATE contacts SET name= ? ,address= ? ,phone= ? ,email= ? WHERE id="+id;
			stmtUpdate = con.prepareStatement(statement);
			
			stmtUpdate.setString(1, contact.name);
			stmtUpdate.setString(2, contact.address);
			stmtUpdate.setString(3, contact.phone);
			stmtUpdate.setString(4, contact.getEmail());
			
			stmtUpdate.execute();
			
			/**
			 * Noch ohne SChutz
			 */
			//int rowsAffected2 = stmtInsert
			//		.executeUpdate("UPDATE dates set date=CURRENT_DATE WHERE id="+id);
			
			statement = "UPDATE dates set date=CURRENT_DATE WHERE id="+id;
			stmtUpdate = con.prepareStatement(statement);
			
			stmtUpdate.execute();
			
			
			con.close();
			stmtUpdate.close();
			System.out.println("ENDUPDATE");
		} catch(Exception e){
			System.out.print(e.getMessage());
		} 
		
	}
	
	/**
	 * Debug purpose only
	 * 
	 * @param args
	 * @throws SQLException
	 */
	public static void main(String[] args) throws SQLException {
		System.out.println("Checking DB Methods:");
		System.out.println("Print Contacts:");
		//printDates();
		System.out.println("Save Contact:");
		try{
			//Connection con = openConnection()
			//printContacts(con);
			//Contact contact = new Contact("Tobias Weck3", "O27-414434502", "+49 731345345 50-24251", "tobias.weck@uni-ulm.de");
			//int id = insertContact(contact);
			//int id2 = insertContact(contact);
			//int id3 = insertContact(contact);
			//int id4 = insertContact(contact);
			//deleteAllContacts();
			//updateContact(new Contact(98,"Daniefdfgl2","test","sdfsd34245","m@m.de"));
			//deleteContact(106);
			//deleteContact(107);
			printContacts();
			
			//List<Contact> contacts = loadContacts();
			//System.out.println(contacts);
			
			//printDates();
			/*
			contact.setId(id);
			contact.setName("Tobi");
			updateContact(contact);
			printContacts();
			printDates();
			deleteContact(id);
			printContacts();
			printDates();*/
			
		}
		catch(Exception m){
			JOptionPane.showMessageDialog(null, m.getMessage());
		}
		
		
	}
}
