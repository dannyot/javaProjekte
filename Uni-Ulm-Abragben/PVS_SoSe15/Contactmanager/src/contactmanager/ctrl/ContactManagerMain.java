/**
 * 
 */
package contactmanager.ctrl;

import contactmanager.data.Contact;
import contactmanager.exeption.EmailException;
import contactmanager.gui.AdressForm;
import contactmanager.util.DbManager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StringReader;
import java.util.*;

import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import uni.ulm.test.DocumentWriter;

/**
 * Diese Klasse stellt den zentralen Einsprungspunkt fuer den Contact Manager
 * dar.
 * 
 * @author Christian Christian Spann
 * 
 */
public class ContactManagerMain {
	public static List<Contact> contacts = new ArrayList<Contact>();
	public static int indexPointer = 0;
	public static File saveFolder = null;

	/**
	 * Die main-Methode der Klasse welche die Anwendung startet.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Anwendung hier starten.

		/*
		 * Liste wird erstellt. Generics werden auch gesetzt.
		 */

		/*
		 * Kontakte werden erstellt.
		 */
		/*
		 * Contact michael = new Contact("Michael", "Fellhornstrasse 14",
		 * "0000000", "daniel@kl-ottmann.de"); Contact edgar = new
		 * Contact("Edgar", "Fellhornstraße 14", "0000000",
		 * "daniel@kl-ottmann.de"); Contact alex = new Contact("Alex",
		 * "Fellhornstraße 19", "00s00000", "daniel@kasl-ottmann.de"); Contact
		 * daniel = new Contact("Daniel", "Fellhornstraße 19", "00s00000",
		 * "daniel@kasl-ottmann.de"); /* erstellte Kontakte werden in die Liste
		 * eingef�gt. Nach jedem einf�gen des Kontaktes, wird die Liste neu
		 * Sortiert.
		 */

		/*
		 * contacts.add(michael); Collections.sort(contacts);
		 * contacts.add(edgar); Collections.sort(contacts); contacts.add(alex);
		 * Collections.sort(contacts); contacts.add(daniel);
		 * Collections.sort(contacts); System.out.println(contacts);
		 * 
		 * /* Die Gui des ContactManagers.
		 */
		AdressForm gui = new AdressForm();
		contacts.add(null);
		gui.start();

	}

	public static void add(String name, String address, String phone,
			String email) {
		if (!name.equals("")) {
			try {
				if (contacts.size() != 0) {
					contacts.add(contacts.size() - 1, new Contact(name,address, phone, email));
				} else {
					contacts.add(contacts.size(), new Contact(name, address,
							phone, email));
				}
				indexPointer += 1;
				// sort();
			} catch (EmailException m) {
				JOptionPane.showConfirmDialog(null, m.getMessage());
			}
		} else {
			JOptionPane.showConfirmDialog(null, "Name muss eingetragens ein!");
		}
		System.out.println(contacts);
	}
	
	public static void addDB(String name, String address, String phone,
			String email){
		if (!name.equals("")) {
			try {
				Contact contact = new Contact(name,address, phone, email);
				int id = DbManager.insertContact(contact);
				contact.setId(id);
				if (contacts.size() != 0) {
					contacts.add(contacts.size() - 1, contact);
				} else {
					contacts.add(contacts.size(), contact);
				}
				indexPointer += 1;
				
				// sort();
			} catch (Exception m) {
				JOptionPane.showConfirmDialog(null, m.getMessage());
			}
		} else {
			JOptionPane.showConfirmDialog(null, "Name muss eingetragens ein!");
		}
		System.out.println(contacts);
	}

	private static void sort() {
		Collections.sort(contacts);
	}

	public static Contact before() {
		System.out.println(indexPointer);
		if (indexPointer == 0) {
			return null;
		} else {
			indexPointer -= 1;
			return contacts.get(indexPointer);
		}

	}

	public static Contact behind() {
		System.out.println(indexPointer);
		if (indexPointer == contacts.size() - 1) {
			return null;
		}
		indexPointer += 1;
		return contacts.get(indexPointer);
	}

	public static Contact delete() {
		if (indexPointer > 0) {
			DbManager.deleteContact(contacts.get(indexPointer).getID());
			contacts.remove(indexPointer);
			indexPointer -= 1;

		} else {
			if (!contacts.isEmpty()) {
				DbManager.deleteContact(contacts.get(indexPointer).getID());
				contacts.remove(indexPointer);
				return contacts.get(indexPointer);
			}

			return null;
		}

		return contacts.get(indexPointer);
	}
	
	public static Contact deleteDB(){
		if (indexPointer > 0) {

			contacts.remove(indexPointer);
			indexPointer -= 1;

		} else {
			if (!contacts.isEmpty()) {
				contacts.remove(indexPointer);
				return contacts.get(indexPointer);
			}

			return null;
		}

		return contacts.get(indexPointer);
	}

	public static void saveContacts(File file) {
		try {
			FileWriter writer = new FileWriter(file);
			String toSave = "";
			for (Contact c : contacts) {
				if (c != null) {
					toSave += c.name + ";" + c.address + ";" + c.phone + ";"
							+ c.getEmail() + "\n";
				}
			}
			writer.write(toSave);
			System.out.println(contacts);
			writer.close();
			saveFolder = file;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

	public static void saveXML(File file) {
		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder dBuilder;
			dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.newDocument();
			Element dir = doc.createElement("directory");
			doc.appendChild(dir);
			for (Contact c : contacts) {
				if (c != null) {
					c.toXML(doc);
				}
			}
			saveFolder = file;
			DocumentWriter.write(doc, file);

		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void saveBinaryStream(File file) {
		try {
			FileOutputStream fout = new FileOutputStream(file);
			ObjectOutputStream oout = new ObjectOutputStream(fout);
			contacts.remove(null);
			oout.writeObject(contacts);
			/**
			 * for(Contact c : contacts){ if(c!=null){ oout.writeObject(c); } }
			 **/
			fout.close();
			oout.close();
			saveFolder = file;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {

		}

	}
	

	public static Contact loadContacts(File file) {
		contacts.clear();
		contacts.add(null);
		if (file == null) {
			saveFolder = null;
			indexPointer = 0;
		} else {
			String readFile = "";

			try {
				FileReader reader = new FileReader(file);
				BufferedReader br = new BufferedReader(reader);
				String zeile = br.readLine();
				while (zeile != null) {
					String[] test = zeile.split(";");
					add(test[0], test[1], test[2], test[3]);
					zeile = br.readLine();
				}
				indexPointer = contacts.size() - 2;
				br.close();
				reader.close();
				saveFolder = file;
				System.out.println(contacts);
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}
		return contacts.get(indexPointer);
	}

	public static Contact loadBinaryStream(File file) {
		try {
			FileInputStream fin = new FileInputStream(file);
			ObjectInputStream oin = new ObjectInputStream(fin);

			contacts.clear();
			contacts = (List<Contact>) oin.readObject();
			contacts.add(null);
			fin.close();
			oin.close();
			System.out.println(contacts);
			indexPointer = contacts.size() - 2;
			saveFolder = file;
			return contacts.get(indexPointer);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return contacts.get(0);
	}

	public static Contact loadXML(File file) {
		Contact c=null;
		try {
			contacts.clear();
			contacts.add(null);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder dBuilder;
			dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(file);
			NodeList nl = doc.getElementsByTagName("Contact");
			
			for(int i = 0; i<nl.getLength();i++){
				c = Contact.create(nl.item(i));
				add(c.name,c.address,c.phone,c.getEmail());
				System.out.print("Index "+i+" ");
				System.out.println(contacts);
			}
			saveFolder = file;
			indexPointer = contacts.size() - 2;
			return contacts.get(indexPointer);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return c;
		}
	}
	
	public static Contact loadDB(){
		contacts.clear();
		System.out.print(contacts.size());
		contacts.add(null);
		contacts = DbManager.loadContacts();
		contacts.add(null);
		System.out.print(contacts.size());
		indexPointer = contacts.size() - 2;
		return contacts.get(indexPointer);
	}
	
	public static void deleteALL(){
		contacts.clear();
		contacts.add(null);
		indexPointer = 0;
		DbManager.deleteAllContacts();
	}
	
	public static void saveDB(Contact cue){
		Contact c = contacts.get(indexPointer);
		c.name = cue.name;
		DbManager.updateContact(c);
	}
}
