package contactmanager.db;

import contactmanager.data.Attribute;

public class SearchContactsTask {
	
	public final DbListener listener;
	public final String searchString;
	public final Attribute attribute;

	public SearchContactsTask(DbListener listener, String searchString, Attribute attribute) {
		this.listener = listener;
		this.searchString = searchString;
		this.attribute = attribute;
	}
}
