package contactmanager.db;

import java.util.EventListener;
import java.util.List;

import contactmanager.data.Contact;

public interface DbListener extends EventListener {

	// notification when contacts were found
	public void contactsFound(List<Contact> contacts);
	
	// notification when an exception has occurred
	public void exceptionOccurred(Exception e);
}
