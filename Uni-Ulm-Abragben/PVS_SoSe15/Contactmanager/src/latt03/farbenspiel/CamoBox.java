package latt03.farbenspiel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class CamoBox {
	public static void main(String args[]){
		String[] farben = {"blue","red","green"};
		JFrame frame = new JFrame();
		
		//frame.setSize(new Dimension(300,400));
		frame.setLayout(new BorderLayout());
		
		
		final JPanel backgroung = new JPanel();
		backgroung.setBackground(Color.BLUE);
		backgroung.setPreferredSize(new Dimension(300,400));
		
		JComboBox dropdown = new JComboBox(farben);
		dropdown.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				JComboBox combo = (JComboBox) e.getSource();
				String item =(String) combo.getSelectedItem();
				if(item == "red"){
					backgroung.setBackground(Color.RED);
				}
				else if(item == "blue"){
					backgroung.setBackground(Color.BLUE);
				}
				else{
					backgroung.setBackground(Color.GREEN);
				}
			}
		});
	
		frame.add(dropdown, BorderLayout.NORTH);
		frame.add(backgroung, BorderLayout.CENTER);
		
		
		frame.pack();
		frame.setVisible(true);
	}
}
