package latt03.farbenspiel;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class Radiobuttons {

	public static void main(String[] args){
		
		
		
		JFrame frame = new JFrame();
		final JPanel panel = new JPanel();
		ButtonGroup group = new ButtonGroup();
		final JRadioButton red = new JRadioButton("red");
		final JRadioButton blue = new JRadioButton("blue");
		final JRadioButton green = new JRadioButton("green");
		
		
		
		
		red.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				panel.setBackground(Color.RED);
				red.setBackground(Color.RED);
				blue.setBackground(Color.RED);
				green.setBackground(Color.RED);
				
			}
		});
		
		blue.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				panel.setBackground(Color.BLUE);
				red.setBackground(Color.BLUE);
				blue.setBackground(Color.BLUE);
				green.setBackground(Color.BLUE);
				
			}
		});
			
		green.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				panel.setBackground(Color.GREEN);
				red.setBackground(Color.GREEN);
				blue.setBackground(Color.GREEN);
				green.setBackground(Color.GREEN);
			}
		});
		
		panel.add(red);
		panel.add(blue);
		panel.add(green);
		
		group.add(red);
		group.add(blue);
		group.add(green);
		
		red.setSelected(true);
		panel.setBackground(Color.RED);
		red.setBackground(Color.RED);
		blue.setBackground(Color.RED);
		green.setBackground(Color.RED);
		
		
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.add(panel);
		frame.setSize(300, 300);
		
		
		
	}


}
