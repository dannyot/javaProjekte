package latt03.MousListener;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JTextArea;

public class Moulisten {
	public static void main(String args[]){
		JFrame frame = new JFrame();
		
		final JTextArea text = new JTextArea("Willkommens Text");
		text.setPreferredSize(new Dimension(600,60));
		
		JButton button = new JButton("button 1");
		final JButton button2 = new JButton("Knopf");
		
		button2.addMouseMotionListener(new MouseMotionListener() {
			
			@Override
			public void mouseMoved(MouseEvent e) {
				button2.setText(""+e.getX()+","+e.getY());
			}
			
			@Override
			public void mouseDragged(MouseEvent e) {
				System.out.println("test");
				
			}
		});
		button2.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				button2.setText("Knopf");
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
		
		button.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				text.setText("mousReleased");
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				text.setText("mousPressed");
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				text.setText("mousExited");
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				text.setText("mousEntered");
				
			}
			@Override
			public void mouseClicked(MouseEvent e){
				System.out.println("ok");
			}
			
		});
		frame.setLayout(new GridLayout(3,1,10,10));
		frame.add(button);
		frame.add(text);
		frame.add(button2);
		frame.pack();
		frame.setVisible(true);
	
	}
}
